﻿using Datalayer.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Reflection.Metadata.Ecma335;

namespace Datalayer.Core
{
    public interface ITenantProvider
    {
        Guid GetTenantId();
    }
    public class TenantProvider : ITenantProvider
    {
        private readonly IHttpContextAccessor _accessor;
        private Guid _currentTenant;

        public TenantProvider(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public Guid GetTenantId()
        {
            var tenantId = _accessor.HttpContext.User.Claims.SingleOrDefault(c => c.Type == "TENANT");
            if (tenantId != null)
            {
                Guid.TryParse(tenantId.Value, out _currentTenant);
            }
            return _currentTenant;
        }
    }
}
