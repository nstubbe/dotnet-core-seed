﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Datalayer.Core
{
    public class ContextDesignFactory : DesignTimeDbContextFactoryBase<Context>
    {
        public ContextDesignFactory() : base("MigrationConnection", "Datalayer")
        { }
        protected override Context CreateNewInstance(DbContextOptions<Context> options)
        {
            return new Context(options);
        }
    }
}
