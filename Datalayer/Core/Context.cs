﻿using Datalayer.Entities;
using Datalayer.Interfaces;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Datalayer.Core
{
    public class Context : IdentityDbContext
    {
        public DbSet<TenantEntity> Tenants { get; set; }

        private readonly ITenantProvider _tenantProvider;

        public Context(DbContextOptions<Context> options, ITenantProvider tenantProvider) : base(options)
        {
            _tenantProvider = tenantProvider;
        }

        internal Context(DbContextOptions<Context> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Add filter based on tenant ID to any entity with ITenantEntity
            var configureTenantMethod = GetType().GetTypeInfo().DeclaredMethods.Single(m => m.Name == nameof(ConfigureTenantFilter));
            var args = new object[] { modelBuilder };

            var tenantEntityTypes = modelBuilder.Model.GetEntityTypes()
                .Where(t => typeof(ITenantEntity).IsAssignableFrom(t.ClrType));

            foreach (var entityType in tenantEntityTypes)
                configureTenantMethod.MakeGenericMethod(entityType.ClrType).Invoke(this, args);
        }

        private void ConfigureTenantFilter<TEntity>(ModelBuilder modelBuilder)
            where TEntity : class, ITenantEntity
        {
            modelBuilder.Entity<TEntity>()
                .HasQueryFilter(e => e.TenantId == _tenantProvider.GetTenantId());
        }

        public override int SaveChanges()
        {
            UpdateEntityDates();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            UpdateEntityDates();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void UpdateEntityDates()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is IEntityDate
                                && ( e.State == EntityState.Added || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                ((IEntityDate) entityEntry.Entity).LastModified = DateTime.Now;

                if (entityEntry.State == EntityState.Added)
                {
                    ((IEntityDate) entityEntry.Entity).Created = DateTime.Now;
                }
            }
        }
    }
}
