﻿using Datalayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Datalayer.Entities
{
    public abstract class EntityBase<T> : IEntityDate
    {
        public T Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime? LastModified { get; set; }
    }
}
