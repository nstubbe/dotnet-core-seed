﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Datalayer.Entities
{
    public class TenantEntity : EntityBase<Guid>
    {
        public string Name { get; set; }
        public string SubDomainUrl { get; set; }
    }
}