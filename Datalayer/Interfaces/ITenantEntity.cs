﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Datalayer.Interfaces
{
    interface ITenantEntity
    {
        Guid TenantId { get; set; }
    }
}
