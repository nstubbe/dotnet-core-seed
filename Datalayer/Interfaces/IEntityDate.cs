﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Datalayer.Interfaces
{
    interface IEntityDate
    {
        DateTime Created { get; set; }
        DateTime? LastModified { get; set; }
    }
}
