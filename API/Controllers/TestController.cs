﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace API.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        public TestController()
        {
            
        }
        [HttpGet]
        public IActionResult Get()
        {
            var identity = User.Identities.FirstOrDefault();
            if (identity == null)
                return Unauthorized();

            var currentEmail = identity.Claims.FirstOrDefault(c => c.Type == "emails");
            if (currentEmail == null)
                return Unauthorized();

            return Ok(new {message = "Authenticated!", UserEmail = currentEmail.Value });
        }
    }
}