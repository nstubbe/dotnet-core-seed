# dotnet-core-seed

This project is supposed to serve as a starting point for a multi-tenant Web API. The main goal is to have an as close to zero-configuration as possible starting point.

## Project overview
* A PostgreSQL container as a development database (port 5432).
* A Seq container for logging (port 5341).
* A .NET Core web api container for our project.
* EF Core datalayer, configured for multi-tenant in a single database. Use ITenantEntity on entities that are supposed to be split by tenant.
* Base entity for EF with automatic updating Created and LastModified fields.
* Authentication using Azure AD B2C.
* Automapper configured and ready for use.
* Hangfire configured and ready for use.

## How to get started

* Make sure you have docker installed on your systen.
* If you want to use the Azure AD B2C authentication, replace the values in appsettings under AzureAdB2C with your own values.
